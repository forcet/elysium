<%-- 
    Document   : index
    Created on : 17/01/2015, 04:54:26 PM
    Author     : Mario
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="es">
    <head>       
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <title>Elysium</title>

        <link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400,600,700&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="css/bootstrap.min.css" type="text/css">
        <link rel="stylesheet" href="css/style.css" type="text/css">
        <link rel="icon" type="image/png" href="img/logout-icon.png" />
    </head>
    <body>
        <header id="main-header">
            <div class="container">
                <div class="row">
                    <div class="col-xs-6 col-sm-4">
                        <a id="main-logo" href="index.jsp" ><img class="pad" src="img/RTMB.png" alt="Reservación de turnos" ></a>
                    </div>
                    <div class="col-xs-6 col-sm-8">           
                        <span><h1>Hospital Elysium</h1></span>
                    </div>           
                </div>
            </div> 
        </header> 
        <div class="wrapper">
            <div class="title">
                <div class="col-xs-12"><h2 style="text-align: center">BIENVENIDO AL ENTORNO DE SOLICITUD DE TURNOS</h2></div>
            </div>
            <article id="w1" class="content"><%--workshop workshop-left--%>
                <div class="well letter" style="max-width: 500px; margin: 0 auto 10px;">
                    <div class="row">
                        <div class="col-xs-12">
                            <form class="form-horizontal" method="post" action="inc/config/val_login.jsp">
                                <div class="form-group">
                                    <label for="inputCorreo" class="col-sm-4 control-label">Usuario</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" name="usuario" placeholder="Usuario" required="required">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputContraseña" class="col-sm-4 control-label">Contraseña</label>
                                    <div class="col-sm-8">
                                        <input type="password" class="form-control" name="password" placeholder="Contraseña" required="required">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-offset-2 col-sm-8">
                                        <label>
                                            <a href="inc/forms/registro.jsp">Registrarse</a>
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-offset-8 col-sm-2">
                                        <button type="submit" class="btn btn-default">Ingresar</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </article>
        </div>
        <footer id="main-footer">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-7">
                        <img class="img-responsive" src="img/imagenPie.png">
                    </div>
                    <div class="col-xs-12 col-sm-5 col-md-5">
                        <p style="color: #009ece">La buena medicina es aquella que mejora el cuerpo y no asalta el bolsillo.</p>
                    </div>
                </div>
            </div>
        </footer>
    </body>
</html>
