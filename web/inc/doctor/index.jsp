<%-- 
    Document   : index
    Created on : 18/01/2015, 02:12:15 AM
    Author     : Mario
--%>
<%
    HttpSession sesionActiva = request.getSession();
    String usuario = "";
    String idusuario = "";
    String rol = "";
    
    if (sesionActiva.getAttribute("idUsuario") == null) {

        response.sendRedirect("../../");
    } else {
        if (sesionActiva.getAttribute("rolUsuario").equals("paciente")) {
            response.sendRedirect("../paciente/");
        } else {
            if (sesionActiva.getAttribute("rolUsuario").equals("admin")) {
                response.sendRedirect("../admin/");
            }
        }

        usuario = (String) sesionActiva.getAttribute("nombreUsuario");
        idusuario = (String) sesionActiva.getAttribute("idUsuario");
        rol = (String) sesionActiva.getAttribute("rolUsuario");

    }
    
    String[] variable =   {"Perfil", "Consultas","Historia","Citas" ,"Recetas","Salir"};
    String[] dir =  {"../../img/perfilDoc.png","../../img/consultas.png","../../img/crearCitas.png","../../img/recordatorio.png","../../img/logout-icon.png","../../img/logout-icon.png"};
    String[] dest =  {"perfil.jsp","#","#","#","#","../config/logout.jsp"};
%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="es">
    <head>
        <jsp:include page="../../inc/config/header_common.jsp" flush="true" />
    </head>
    <body>
        <jsp:include page="../../inc/config/header.jsp" flush="true" />  
        <div class="wrapper">
            <div class="title">
                <div class="col-xs-12"><h2 style="text-align: center">DOCTOR</h2></div>
            </div>
            <div id="content-list"><%--workshop-list --%>
                <article id="w1" class="content content-left"><%--workshop workshop-left--%>
                    <div class="well" style="max-width: 900px; margin: 0 auto 10px;">
                        <div class="row">                            
                            <div class="col-xs-10 col-md-10 col-lg-10" style="margin: 40px ;">
                                <div class="row" style="text-align: center">
                                    <% 
                                        for(int i=0; i<variable.length;i++){
                                            //out.write(variable[i]);
                                            out.write("<div class=\"col-xs-6 col-sm-4 col-md-4 col-lg-4\">");
                                            out.write("<a href="+dest[i]+" onclick=\"\" style=\"color: #000\" title="+variable[i]+">");
                                            out.write("<div class=\"panel panel-info\">");
                                            out.write("<div class=\"row\">");
                                            out.write("<div class=\"col-xs-12\"><br>");
                                            out.write("<img src="+dir[i]+" class=\"img-responsive\" alt="+"Imagen "+variable[i]+">");
                                            out.write("</div>");
                                            out.write("</div>");
                                            out.write("<div class=\"row\">");
                                            out.write("<div class=\"col-xs-12\"><h3><strong>"+variable[i]+"</strong></h3></div>");
                                            out.write("</div>");
                                            out.write("</div>");
                                            out.write("</a>");
                                            out.write("</div>");                                            
                                        }
                                    %>                                
                                <hr>    
                                </div>
                                
                            </div>
                                    <div class="col-xs-12 col-md-12 col-lg-12" style="text-align: right"><p><h3>BIENVENIDO <% out.write(usuario);%></h3></p></div>
                        </div>
                    </div>
                </article>
            </div>
        </div>
        <jsp:include page="../../inc/config/footer.jsp" flush="true" />
        <jsp:include page="../../inc/config/footer_common.jsp" flush="true" />
    </body>
</html>





