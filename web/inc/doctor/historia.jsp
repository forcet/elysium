<%-- 
    Document   : historia
    Created on : 27/01/2015, 09:10:02 AM
    Author     : Mario
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    HttpSession sesionActiva = request.getSession();
    String usuario = "";
    String idusuario = "";
    String rol = "";
    
    if (sesionActiva.getAttribute("idUsuario") == null) {

        response.sendRedirect("../../");
    } else {
        if (sesionActiva.getAttribute("rolUsuario").equals("paciente")) {
            response.sendRedirect("../paciente/");
        } else {
            if (sesionActiva.getAttribute("rolUsuario").equals("admin")) {
                response.sendRedirect("../admin/");
            }
        }

        usuario = (String) sesionActiva.getAttribute("nombreUsuario");
        idusuario = (String) sesionActiva.getAttribute("idUsuario");
        rol = (String) sesionActiva.getAttribute("rolUsuario");

    }
    
    
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Hello World!</h1>
    </body>
</html>
