<%-- 
    Document   : perfil
    Created on : 25/01/2015, 10:27:39 PM
    Author     : Mario
--%>

<%@page import="java.io.IOException"%>
<%@page import="java.net.MalformedURLException"%>
<%@page import="org.json.simple.JSONObject"%>
<%@page import="org.json.simple.JSONArray"%>
<%@page import="org.json.simple.parser.JSONParser"%>
<%@page import="java.io.InputStreamReader"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="java.net.HttpURLConnection"%>
<%@page import="java.net.URL"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    HttpSession sesionActiva = request.getSession();
    String usuario = "";
    String idusuario = "";
    String rol = "";

    if (sesionActiva.getAttribute("idUsuario") == null) {

        response.sendRedirect("../../");
    } else {
        if (sesionActiva.getAttribute("rolUsuario").equals("doctor")) {
            response.sendRedirect("../doctor/");
        } else {
            if (sesionActiva.getAttribute("rolUsuario").equals("admin")) {
                response.sendRedirect("../admin/");
            }
        }

        usuario = (String) sesionActiva.getAttribute("nombreUsuario");
        idusuario = (String) sesionActiva.getAttribute("idUsuario");
        rol = (String) sesionActiva.getAttribute("rolUsuario");

    }
    
    String[] provincias = {"Azuay", "Bolivar", "Carchi", "Cañar", "Chimborazo", "Cotopaxi", "El Oro", "Esmeraldas", "Galápagos", "Guayas", "Imbabura", "Loja", "Los Ríos", "Morona Santiago", "Napo", "Orellana", "Pastaza", "Pichincha", "Santa Elena", "Santo Domingo de los Tsáchilas", "Sucumbíos", "Tungurahua", "Zamora Chinchipe"};
    String ced="";String[] priNom={};String[] priApe={};String sex="";String sangre="";String[] birth={};String provim="";String cant="";String parro="";String bar="";String mail="";String phone="";String user="";String pass="";
    
    String[] month = {"Mes","Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
%>

<!DOCTYPE html>
<html lang="es">
    <head>
        <jsp:include page="../../inc/config/header_common.jsp" flush="true" />
    </head>
    <body>
        <jsp:include page="../../inc/config/header.jsp" flush="true" />  
        <div class="wrapper">
            <div id="content-list"><%--workshop-list --%>
                <article id="w1" class="content content-left"><%--workshop workshop-left--%>
                    <div class="well" style="max-width: 900px; margin: 0 auto 10px;">
                        <div class="row">  

                            <!--                        <div class="col-xs-12"><h2 style="text-align: center">PERFIL</h2></div>-->

                            <div class="col-xs-10 col-md-10 col-lg-10" style="margin: 40px ;">
                                <div class="col-xs-12">
                                    <div class="btn-group btn-group-justified" role="group">
                                        <div class="btn-group" role="group"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></div>
                                        <div class="btn-group" role="group"><h2>PERFIL</h2></div>
                                        <div class="btn-group" role="group">                                           
                                            <button type="button" class="btn btn-success letter" data-toggle="modal" data-target=".bs-example-modal-lg">Editar <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> </button>
                                        </div>
                                    </div><br>
                                </div>
                                <div class="row" style="text-align: center">
                                    <%
                                        String servicioUrl = "http://localhost:8080/servicios/webresources/persona/"+idusuario;

                                        try {

                                            URL url = new URL(servicioUrl);
                                            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                                            conn.setRequestMethod("GET");
                                            conn.setRequestProperty("Accept", "application/json");

                                            conn.setRequestProperty("Accept-Charset", "UTF-8;q=0.7,*;q=0.7");
                                            if (conn.getResponseCode() != 200) {
                                                throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
                                            }
                                            BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
                                            String output;

                                            while ((output = br.readLine()) != null) {

                                                JSONParser parser = new JSONParser();
                                                Object obj = parser.parse(output);
                                                JSONObject datos = (JSONObject) obj;
                                                //out.print(datos);
                                                int contador = datos.size();
//                                                out.print(obj);
                                                //out.print(contador);
                                                if (contador == 0) {
                                                    out.print("<strong>No se obtuvo resultados...</strong>");
                                                } else {
                                                  ///Personales
                                                        ced = datos.get("cedula").toString();
                                                        out.print("<div class=\"col-xs-12 col-md-6\">");
                                                        out.print("<div class=\"row\">");
                                                        out.print("<div class=\"panel panel-info\">");
                                                        out.print("<div class=\"row\" style=\"margin-left: 20px\">");
                                                        out.print("<div class=\"col-xs-2\"><h2><b><span class=\"glyphicon glyphicon-list-alt\" aria-hidden=\"true\"></b></h2></div>");
                                                        out.print("<div class=\"col-xs-10\"><h2><b> Datos Personales</b></h2></div>");
                                                        out.print("</div><br>");
                                                        out.print("<div class=\"row letter\" style=\"margin-left: 20px\">");
                                                        out.print("<div class=\"col-xs-12\" style=\"text-align: left;\"><b>Cedula:</b> " + datos.get("cedula") + "</div>");
                                                        out.print("</div><br>");
                                                        
                                                        String n = datos.get("nombres").toString();
                                                        priNom = n.split(" ");
                                                        String a = datos.get("apellidos").toString();
                                                        priApe = a.split(" ");
                                                        out.print("<div class=\"row letter\" style=\"margin-left: 20px\">");
                                                        out.print("<div class=\"col-xs-12\" style=\"text-align: left;\"><b>Nombres: </b>" + datos.get("nombres") + " " + datos.get("apellidos") + "</div>");
                                                        out.print("</div><br>");
                                                        
                                                        mail = datos.get("correo").toString();
                                                        out.print("<div class=\"row letter\" style=\"margin-left: 20px\">");
                                                        out.print("<div class=\"col-xs-12\" style=\"text-align: left;\"><b>Correo: </b>" + datos.get("correo") + "</div>");
                                                        out.print("</div><br>");
                                                        
                                                        phone = datos.get("telefono").toString();
                                                        out.print("<div class=\"row letter\" style=\"margin-left: 20px\">");
                                                        out.print("<div class=\"col-xs-12\" style=\"text-align: left;\"><b>Telefono: </b>" + datos.get("telefono") + "</div>");
                                                        out.print("</div><br>");
                                                        
                                                        sangre = datos.get("tipoSangre").toString();
                                                        out.print("<div class=\"row letter\" style=\"margin-left: 20px\">");
                                                        out.print("<div class=\"col-xs-12\" style=\"text-align: left;\"><b>Tipo de Sangre: </b>" + datos.get("tipoSangre") + "</div>");
                                                        out.print("</div><br>");
                                                        
                                                        sex = datos.get("sexo").toString();
                                                        out.print("<div class=\"row letter\" style=\"margin-left: 20px\">");
                                                        out.print("<div class=\"col-xs-12\" style=\"text-align: left;\"><b>Sexo: </b>" + datos.get("sexo") + "</div>");
                                                        out.print("</div><br>");

                                                        String cumple = datos.get("fechaNacimiento").toString();
                                                        String[] c = cumple.split("T");
                                                        birth = c[0].toString().split("-");
                                                        out.print("<div class=\"row letter\" style=\"margin-left: 20px\">");
                                                        out.print("<div class=\"col-xs-12\" style=\"text-align: left;\"><b>Cumpleaños: </b>" + c[0] + "</div>");
                                                        out.print("</div><br>");

                                                        out.print("</div>");
                                                        out.print("</div>");
                                                        out.print("</div>");

                                                        //Provincias
                                                        out.print("<div class=\"col-xs-12 col-md-6\">");
                                                        out.print("<div class=\"row\">");
                                                        out.print("<div class=\"panel panel-info\">");

                                                        out.print("<div class=\"row\" style=\"margin-left: 20px\">");
                                                        out.print("<div class=\"col-xs-2\"><h2><b><span class=\"glyphicon glyphicon-globe\" aria-hidden=\"true\"></b></h2></div>");
                                                        out.print("<div class=\"col-xs-10\"><h2><b> Datos de Vivienda</b></h2></div>");
                                                        out.print("</div><br>");    
                                                        
                                                        provim=datos.get("provincia").toString();
                                                        out.print("<div class=\"row letter\" style=\"margin-left: 20px\">");
                                                        out.print("<div class=\"col-xs-12\" style=\"text-align: left;\"><b>Provincia: </b>" + datos.get("provincia") + "</div>");
                                                        out.print("</div><br>");

                                                        cant=datos.get("canton").toString();
                                                        out.print("<div class=\"row letter\" style=\"margin-left: 20px\">");
                                                        out.print("<div class=\"col-xs-12\" style=\"text-align: left;\"><b>Canton: </b>" + datos.get("canton") + "</div>");
                                                        out.print("</div><br>");

                                                        parro=datos.get("parroquia").toString();
                                                        out.print("<div class=\"row letter\" style=\"margin-left: 20px\">");
                                                        out.print("<div class=\"col-xs-12\" style=\"text-align: left;\"><b>Parroquia: </b>" + datos.get("parroquia") + "</div>");
                                                        out.print("</div><br>");

                                                        bar=datos.get("barrio").toString();
                                                        out.print("<div class=\"row letter\" style=\"margin-left: 20px\">");
                                                        out.print("<div class=\"col-xs-12\" style=\"text-align: left;\"><b>Barrio: </b>" + datos.get("barrio") + "</div>");
                                                        out.print("</div><br>");

                                                        out.print("</div>");
                                                        out.print("</div>");
                                                        out.print("</div>");
                                                        user=datos.get("usuario").toString();
                                                        pass=datos.get("contrasena").toString();

  
                                                }
                                            }

                                            conn.disconnect();

                                        } catch (MalformedURLException e) {

                                            out.print("Error con servidor" + e.getMessage());

                                        } catch (IOException e) {

                                            out.print("Error con servidor: " + e.getMessage());

                                        }

                                    %>
                                </div>

                            </div>
                            <div class="col-xs-12 col-md-12 col-lg-12" style="text-align: right"><p><h3>BIENVENIDO <% out.write(usuario);%></h3></p></div>
                        </div>
                    </div>
                </article>
            </div>
        </div>

 

<div class="modal fade bs-example-modal-lg" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Editar Datos Personales</h4>
                    </div>
                    <div class="modal-body letter">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="input-group">
                                    <span class="input-group-addon" id="sizing-addon3">Cédula</span>
                                    <input type="text" class="form-control" id="cedula" placeholder="Cédula" aria-describedby="sizing-addon3" maxlength="10" value="<% out.print(ced); %>" onkeypress="return soloNumero(event)">
                                    <input type="hidden" id="idPaciente" value="<%out.print(idusuario);%>">
                                    <input type="hidden" id="passw" value="<%out.print(pass);%>">
                                </div>
                            </div>

                            <div class="col-xs-12">
                                <div class="input-group">
                                    <span class="input-group-addon" id="sizing-addon3">Primer Nombre</span>
                                    <input type="text" class="form-control" id="primerNombre" placeholder="Primer Nombre" aria-describedby="sizing-addon3" value="<% out.print(priNom[0]);%>" onkeypress="return soloLetras(event)">
                                </div>
                            </div>

                            <div class="col-xs-12">
                                <div class="input-group">
                                    <span class="input-group-addon" id="sizing-addon3">Segundo Nombre</span>
                                    <input type="text" class="form-control" id="segundoNombre" placeholder="Segundo Nombre" aria-describedby="sizing-addon3" value="<% out.print(priNom[1]);%>" onkeypress="return soloLetras(event)">
                                </div>
                            </div>
                            <div class="col-xs-12">
                                <div class="input-group">
                                    <span class="input-group-addon" id="sizing-addon3">Apellido Paterno</span>
                                    <input type="text" class="form-control" id="apellidoP" placeholder="Apellido Paterno" aria-describedby="sizing-addon3" value="<% out.print(priApe[0]);%>" onkeypress="return soloLetras(event)">
                                </div>
                            </div>

                            <div class="col-xs-12">
                                <div class="input-group">
                                    <span class="input-group-addon" id="sizing-addon3">Apellido Materno</span>
                                    <input type="text" class="form-control" id="apellidoM" placeholder="Apellido Materno" aria-describedby="sizing-addon3" value="<% out.print(priApe[0]);%>" onkeypress="return soloLetras(event)">
                                </div>
                            </div>

                            <div class="col-xs-12">
                                <div class="input-group">
                                    <span class="input-group-addon" id="sizing-addon3">Sexo</span>
                                    <% 
                                    if(sex.equals("Masculino")){
                                    %>
                                    <select name="sexo" id="sexo" class="form-control" onchange="">
                                        <option>-- Seleccione una opción --</option>
                                        <option value="Masculino" selected="1">Masculino</option>
                                        <option value="Femenino">Femenino</option>
                                        <option value="Indefinido">Indefinido</option>
                                    </select>                                       
                                    <%
                                    }else if(sex.equals("Femenino")){
                                    %>
                                    <select name="sexo" id="sexo" class="form-control" onchange="">
                                        <option>-- Seleccione una opción --</option>
                                        <option value="Masculino">Masculino</option>
                                        <option value="Femenino" selected="1">Femenino</option>
                                        <option value="Indefinido">Indefinido</option>
                                    </select>
                                    <%
                                    }else{                                   
                                    %>
                                    <select name="sexo" id="sexo" class="form-control" onchange="">
                                        <option>-- Seleccione una opción --</option>
                                        <option value="Masculino">Masculino</option>
                                        <option value="Femenino">Femenino</option>
                                        <option value="Indefinido">Indefinido</option>
                                    </select>
                                    <%
                                    }
                                    %>
                                </div>
                            </div>
                            <div class="col-xs-12">
                                <div class="input-group">
                                    <span class="input-group-addon" id="sizing-addon3">Tipo de Sangre</span>
                                    <input type="text" class="form-control" id="sangre" placeholder="Tipo de Sangre" value="<% out.print(sangre);%>" aria-describedby="sizing-addon3">
                                </div>
                            </div>
                            <div class="col-xs-12">
                                <div class="input-group">
                                    <span class="input-group-addon" id="sizing-addon3">Cumpleaños</span>
                                    <div class="btn-group btn-group-justified" role="group" aria-label="...">

                                        <select name="meses" id="meses" class="form-control">
                                            <%
                                            int j = 0;
                                            for(String months: month){                               
                                                if(j==Integer.parseInt(birth[1])){
                                                    out.print("<option value="+j+" selected=\"1\">"+months+"</option>");
                                                }else{
                                                    out.print("<option value="+j+">"+months+"</option>");
                                                }
                                                j++;
                                            }
                                            %>
                                        </select>
                                        <select name="dia" id="dia" class="form-control">
                                            <option value="0">Día</option>
                                            <%
                                                for(int x=1;x<=31;x++){
                                                    if(x==Integer.parseInt(birth[2])){
                                                        out.print("<option value="+x+" selected=\"1\">"+x+"</option>");
                                                    }else{
                                                        out.print("<option value="+x+">"+x+"</option>");
                                                    }
                                                }
                                            %>
                                        </select>
                                        <select name="anio" id="anio" class="form-control">
                                            <option value="0">Año</option>
                                            <%
                                                for(int an=2015;an>=1905;an--){
                                                    if(an==Integer.parseInt(birth[0])){
                                                        out.print("<option value="+an+" selected=\"1\">"+an+"</option>");
                                                    }else{
                                                        out.print("<option value="+an+">"+an+"</option>");
                                                    }
                                                }
                                            %>
                                        </select>

                                    </div>                                            
                                </div>
                            </div>
                            <form name="form">
                                <div class="col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon" id="sizing-addon3">Provincia</span>
                                        <select name="select" class="form-control" id="provincias" onchange="slctryole(this, this.form.cantones)">
                                            <option></option>
                                            <%
                                                for (String prov : provincias) {
                                                    if(prov.equals(provim)){
                                                        out.write("<option value='" + prov.replace(" ", "_").toLowerCase() + "' selected=''>" + prov + "</option>");
                                                    }else{
                                                        out.write("<option value='" + prov.replace(" ", "_").toLowerCase() + "'>" + prov + "</option>");
                                                    }
                                                    
                                                }
                                            %>

                                        </select>

                                    </div>
                                </div>
                                <div class="col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon" id="sizing-addon3">Cantones</span>
                                        <select name="cantones" id="cantones" class="form-control" onchange="">
                                            <option value="<% out.print(cant); %>"><% out.print(cant); %></option>                                                
                                        </select>                                       
                                    </div>
                                </div>
                            </form>
                            <div class="col-xs-12">
                                <div class="input-group">
                                    <span class="input-group-addon" id="sizing-addon3">Parroquia</span>
                                    <input type="correo" class="form-control" id="parroquia" placeholder="Parroquia" aria-describedby="sizing-addon3" value="<% out.print(parro);%>" onkeypress="return caractNumEs(event)">
                                </div>
                            </div>
                            <div class="col-xs-12">
                                <div class="input-group">
                                    <span class="input-group-addon" id="sizing-addon3">Barrio</span>
                                    <input type="text" class="form-control" id="barrio" placeholder="Barrio" aria-describedby="sizing-addon3" value="<% out.print(bar);%>" onkeypress="return caractNumEs(event)">
                                </div>
                            </div>
                            <div class="col-xs-12">
                                <div class="input-group">
                                    <span class="input-group-addon" id="sizing-addon3">Correo</span>
                                    <input type="correo" class="form-control" id="correo" placeholder="Correo" aria-describedby="sizing-addon3" value="<% out.print(mail);%>" onkeypress="return correo(event)">
                                </div>
                            </div>
                            <div class="col-xs-12">
                                <div class="input-group">
                                    <span class="input-group-addon" id="sizing-addon3">Teléfono</span>
                                    <input type="text" class="form-control" id="telefono" placeholder="Teléfono" aria-describedby="sizing-addon3" value="<% out.print(phone);%>" onkeypress="return soloNumero(event)">
                                </div>
                            </div>
                            <div class="col-xs-12">
                                <div class="input-group">
                                    <span class="input-group-addon" id="sizing-addon3">Usuario</span>
                                    <input type="text" class="form-control" id="usuario" placeholder="Usuario" aria-describedby="sizing-addon3" value="<% out.print(user);%>" onkeypress="return caractNum(event)">
                                </div>
                            </div>
                            <div class="col-xs-12">
                                <div class="input-group">
                                    <span class="input-group-addon" id="sizing-addon3">Contraseña</span>
                                    <input type="password" class="form-control" id="clave" placeholder="Contraseña" aria-describedby="sizing-addon3">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
<!--                        <button type="button" id="enviar" class="btn btn-default">Registrarse</button>-->
                        <button type="button" id="datosPa" class="btn btn-primary">Guardar</button>
                    </div>
                </div>
            </div>
        </div>
        

        <script src="../../js/elysium/provinicias.js" type="text/javascript"></script>
        <jsp:include page="../../inc/config/footer.jsp" flush="true" />
        <jsp:include page="../../inc/config/footer_common.jsp" flush="true" />
        <script src="../../js/md5/md5.min.js" type="text/javascript"></script>
        <script src="../../js/elysium/configuraciones.js" type="text/javascript"></script>
        <script src="../../js/elysium/paciente.js" type="text/javascript"></script>
    </body>
</html>
