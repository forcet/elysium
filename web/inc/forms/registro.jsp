<%-- 
    Document   : registro
    Created on : 18/01/2015, 04:42:27 PM
    Author     : Mario
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    String[] provincias = {"Azuay", "Bolivar", "Carchi", "Cañar", "Chimborazo", "Cotopaxi", "El Oro", "Esmeraldas", "Galápagos", "Guayas", "Imbabura", "Loja", "Los Ríos", "Morona Santiago", "Napo", "Orellana", "Pastaza", "Pichincha", "Santa Elena", "Santo Domingo de los Tsáchilas", "Sucumbíos", "Tungurahua", "Zamora Chinchipe"};
    String[] month = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
%>
<!DOCTYPE html>
<html lang="es">
    <head>
        <jsp:include page="../../inc/config/header_common.jsp" flush="true" />
    </head>
    <body>
        <jsp:include page="../../inc/config/header.jsp" flush="true" />  
        <div class="wrapper">
            <div class="title">
                <div class="col-xs-12"><h2 style="text-align: center">Registro de Nuevo Usuario</h2></div>
            </div>
            <div id="content-list"><%--workshop-list --%>
                <article id="w1" class="content content-left"><%--workshop workshop-left--%>
                    <div class="well letter" style="max-width: 900px; margin: 0 auto 10px;">
                        <div class="row"> 
                            <div class="col-xs-2 col-md-2 col-lg-2"></div>
                            <div class="col-xs-8 col-md-8 col-lg-8" style="text-align: center">
                                <div class="row">

                                    <div class="col-xs-12">
                                        <div class="input-group">
                                            <span class="input-group-addon" id="sizing-addon3">Cédula</span>
                                            <input type="text" class="form-control" id="cedula" placeholder="Cédula" aria-describedby="sizing-addon3" maxlength="10" onkeypress="return soloNumero(event)">
                                        </div>
                                    </div>

                                    <div class="col-xs-12">
                                        <div class="input-group">
                                            <span class="input-group-addon" id="sizing-addon3">Primer Nombre</span>
                                            <input type="text" class="form-control" id="primerNombre" placeholder="Primer Nombre" aria-describedby="sizing-addon3" onkeypress="return soloLetras(event)">
                                        </div>
                                    </div>

                                    <div class="col-xs-12">
                                        <div class="input-group">
                                            <span class="input-group-addon" id="sizing-addon3">Segundo Nombre</span>
                                            <input type="text" class="form-control" id="segundoNombre" placeholder="Segundo Nombre" aria-describedby="sizing-addon3" onkeypress="return soloLetras(event)">
                                        </div>
                                    </div>
                                    <div class="col-xs-12">
                                        <div class="input-group">
                                            <span class="input-group-addon" id="sizing-addon3">Apellido Paterno</span>
                                            <input type="text" class="form-control" id="apellidoP" placeholder="Apellido Paterno" aria-describedby="sizing-addon3" onkeypress="return soloLetras(event)">
                                        </div>
                                    </div>

                                    <div class="col-xs-12">
                                        <div class="input-group">
                                            <span class="input-group-addon" id="sizing-addon3">Apellido Materno</span>
                                            <input type="text" class="form-control" id="apellidoM" placeholder="Apellido Materno" aria-describedby="sizing-addon3" onkeypress="return soloLetras(event)">
                                        </div>
                                    </div>

                                    <div class="col-xs-12">
                                        <div class="input-group">
                                            <span class="input-group-addon" id="sizing-addon3">Sexo</span>
                                            <select name="sexo" id="sexo" class="form-control" onchange="">
                                                <option>-- Seleccione una opción --</option>
                                                <option value="Masculino">Masculino</option>
                                                <option value="Femenino">Femenino</option>
                                                <option value="Indefinido">Indefinido</option>
                                            </select>                                       
                                        </div>
                                    </div>
                                    <div class="col-xs-12">
                                        <div class="input-group">
                                            <span class="input-group-addon" id="sizing-addon3">Tipo de Sangre</span>
                                            <input type="text" class="form-control" id="sangre" placeholder="Tipo de Sangre" aria-describedby="sizing-addon3">
                                        </div>
                                    </div>
                                    <div class="col-xs-12">
                                        <div class="input-group">
                                            <span class="input-group-addon" id="sizing-addon3">Cumpleaños</span>
                                            <div class="btn-group btn-group-justified" role="group" aria-label="...">

                                                <select name="meses" id="meses" class="form-control">
                                                    <option value="0" selected="1">Mes</option>
                                                    <%
                                                        int j = 1;
                                                        for (String months : month) {
                                                            out.print("<option value=" + j + ">" + months + "</option>");
                                                            j++;
                                                        }
                                                    %>
                                                </select>
                                                <select name="dia" id="dia" class="form-control">
                                                    <option value="0" selected="1">Día</option>
                                                    <%
                                                        for (int x = 1; x <= 31; x++) {
                                                            out.print("<option value=" + x + ">" + x + "</option>");
                                                        }
                                                    %>                                                    
                                                </select>
                                                <select name="anio" id="anio" class="form-control">
                                                    <option value="0" selected="1">Año</option>
                                                    <%
                                                        for (int an = 2015; an >= 1905; an--) {
                                                            out.print("<option value=" + an + ">" + an + "</option>");
                                                        }
                                                    %>
                                                </select>

                                            </div>                                            
                                        </div>
                                    </div>
                                    <form name="form">
                                        <div class="col-xs-12">
                                            <div class="input-group">
                                                <span class="input-group-addon" id="sizing-addon3">Provincia</span>
                                                <select name="select" class="form-control" id="provincias" onchange="slctryole(this, this.form.cantones)">
                                                    <option></option>
                                                    <%
                                                        for (String prov : provincias) {
                                                            out.write("<option value='" + prov.replace(" ", "_").toLowerCase() + "'>" + prov + "</option>");
                                                        }
                                                    %>

                                                </select>

                                            </div>
                                        </div>
                                        <div class="col-xs-12">
                                            <div class="input-group">
                                                <span class="input-group-addon" id="sizing-addon3">Cantones</span>
                                                <select name="cantones" id="cantones" class="form-control" onchange="">
                                                    <option></option>                                                
                                                </select>                                       
                                            </div>
                                        </div>
                                    </form>
                                    <div class="col-xs-12">
                                        <div class="input-group">
                                            <span class="input-group-addon" id="sizing-addon3">Parroquia</span>
                                            <input type="correo" class="form-control" id="parroquia" placeholder="Parroquia" aria-describedby="sizing-addon3" onkeypress="return caractNumEs(event)">
                                        </div>
                                    </div>
                                    <div class="col-xs-12">
                                        <div class="input-group">
                                            <span class="input-group-addon" id="sizing-addon3">Barrio</span>
                                            <input type="text" class="form-control" id="barrio" placeholder="Barrio" aria-describedby="sizing-addon3" onkeypress="return caractNumEs(event)">
                                        </div>
                                    </div>
                                    <div class="col-xs-12">
                                        <div class="input-group">
                                            <span class="input-group-addon" id="sizing-addon3">Correo</span>
                                            <input type="correo" class="form-control" id="correo" placeholder="Correo" aria-describedby="sizing-addon3" onkeypress="return correo(event)">
                                        </div>
                                    </div>
                                    <div class="col-xs-12">
                                        <div class="input-group">
                                            <span class="input-group-addon" id="sizing-addon3">Teléfono</span>
                                            <input type="text" class="form-control" id="telefono" placeholder="Teléfono" aria-describedby="sizing-addon3" onkeypress="return soloNumero(event)">
                                        </div>
                                    </div>
                                    <div class="col-xs-12">
                                        <div class="input-group">
                                            <span class="input-group-addon" id="sizing-addon3">Usuario</span>
                                            <input type="text" class="form-control" id="usuario" placeholder="Usuario" aria-describedby="sizing-addon3" onkeypress="return caractNum(event)">
                                        </div>
                                    </div>
                                    <div class="col-xs-12">
                                        <div class="input-group">
                                            <span class="input-group-addon" id="sizing-addon3">Contraseña</span>
                                            <input type="password" class="form-control" id="clave" placeholder="Contraseña" aria-describedby="sizing-addon3">
                                        </div>
                                    </div>
                                    <div class="col-xs-12"><br>
                                        <div class="btn-group btn-group-justified" role="group" aria-label="...">
                                            <div class="btn-group" role="group">
                                                <button type="button" id="enviar" class="btn btn-default">Registrarse</button>
                                            </div>
                                            <div class="btn-group" role="group">
                                                <button type="button" onclick="history.back();" class="btn btn-default">Cancelar</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="col-xs-2 col-md-2 col-lg-2"></div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-4"></div>
                    </div>
                </article>
            </div>
        </div>



        <script src="../../js/elysium/provinicias.js" type="text/javascript"></script>
        <jsp:include page="../../inc/config/footer.jsp" flush="true" />
        <jsp:include page="../../inc/config/footer_common.jsp" flush="true" />
        <script src="../../js/md5/md5.min.js" type="text/javascript"></script>
        <script src="../../js/elysium/configuraciones.js" type="text/javascript"></script>
        <script src="../../js/elysium/registro.js" type="text/javascript"></script>
    </body>
</html>
