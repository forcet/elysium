<%-- 
    Document   : header_common
    Created on : 17/01/2015, 04:53:51 PM
    Author     : Mario
--%>

<!--<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">-->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<title>Elysium</title>

<!--<link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400,600,700&subset=latin,latin-ext' rel='stylesheet' type='text/css'>-->
<link rel="stylesheet" href="../../css/bootstrap.min.css" type="text/css">
<link rel="stylesheet" href="../../css/style.css" type="text/css">
<link rel="icon" type="image/png" href="../../img/logout-icon.png" />