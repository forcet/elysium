<%-- 
    Document   : val_login
    Created on : 25/01/2015, 01:21:12 AM
    Author     : Mario
--%>
<%@page import="ec.edu.utpl.elysium.funciones.funciones"%>
<%@page session='true' %>
<%@page import="java.io.IOException"%>
<%@page import="java.net.MalformedURLException"%>
<%@page import="java.io.InputStreamReader"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="java.net.HttpURLConnection"%>
<%@page import="java.net.URL"%>

<%
    boolean registrado = false;
    funciones func = new funciones();
    String usuario = request.getParameter("usuario");
    String contrasenia = request.getParameter("password");;
    String nombreUsuario = "";
    String rolUsuario = "";
    String idUsuario = "";

    String loginUrl = "http://localhost:8080/servicios/webresources/persona/login/";

    contrasenia=func.getMD5(contrasenia);
 //out.write(usuario+" - "+contrasenia);
    try {
        URL url = new URL(loginUrl + usuario + "/" + contrasenia);

        HttpURLConnection conex = (HttpURLConnection) url.openConnection();
        conex.setRequestMethod("GET");
        conex.setRequestProperty("Accept", "text");
        conex.setRequestProperty("Accept-Charset", "UTF-8;q=0.7,*;q=0.7");
        if (conex.getResponseCode() != 200) {
            throw new RuntimeException("Failed : HTTP error code : " + conex.getResponseCode());
        }

        BufferedReader buffer = new BufferedReader(new InputStreamReader(
                (conex.getInputStream())));

        String output;

        while ((output = buffer.readLine()) != null) {
            if (!out.equals("")) {

                String[] valores = output.split("-");
                registrado = true;
                idUsuario = valores[0];
                nombreUsuario = valores[1];
                rolUsuario = valores[2];
                out.write(rolUsuario);
            }

        }

        conex.disconnect();

    } catch (MalformedURLException e) {

    } catch (IOException e) {}

    if (registrado) {
        HttpSession sesionActiva = request.getSession();
        sesionActiva.setAttribute("idUsuario", idUsuario);
        sesionActiva.setAttribute("nombreUsuario", nombreUsuario);
        sesionActiva.setAttribute("rolUsuario", rolUsuario);
        if (rolUsuario.equals("admin")) {
            response.sendRedirect("../administrador/");
        } else {
            if (rolUsuario.equals("paciente")) {
                response.sendRedirect("../paciente/");
            } else {
                if (rolUsuario.equals("doctor")) {
                    response.sendRedirect("../doctor/");
                } else {
                    response.sendRedirect("../../");
                }

            }
        }
    } else {
        response.sendRedirect("../../");
    }
%>