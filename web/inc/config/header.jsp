<%-- 
    Document   : header
    Created on : 17/01/2015, 04:53:35 PM
    Author     : Mario
--%>

<header id="main-header">
    <div class="container">
        <div class="row">
            <div class="col-xs-6 col-sm-4">
                <a id="main-logo" href="index.jsp" ><img class="pad" src="../../img/RTMB.png" alt="Reservación de turnos" ></a>
            </div>
            <div class="col-xs-6 col-sm-8">           
                <span><h1>Hospital Elysium</h1></span>
            </div>           
        </div>
    </div> 
</header>
