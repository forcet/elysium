<%-- 
    Document   : footer
    Created on : 17/01/2015, 04:52:34 PM
    Author     : Mario
--%>
<footer id="main-footer">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-7">
                <img class="img-responsive" src="../../img/imagenPie.png" alt="hospital elysium">
            </div>
            <div class="col-xs-12 col-sm-5 col-md-5">
                <p style="color: #009ece">La buena medicina es aquella que mejora el cuerpo y no asalta el bolsillo.</p>
            </div>
        </div>
    </div>
</footer>