<%-- 
    Document   : asignarHorarios
    Created on : 27/01/2015, 12:10:18 PM
    Author     : Mario
--%>
<%@page import="org.json.simple.JSONArray"%>
<%@page import="java.io.IOException"%>
<%@page import="java.net.MalformedURLException"%>
<%@page import="org.json.simple.JSONObject"%>
<%@page import="org.json.simple.parser.JSONParser"%>
<%@page import="java.io.InputStreamReader"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="java.net.HttpURLConnection"%>
<%@page import="java.net.URL"%>

<%
    
    String especialidad = (String) request.getParameter("doctor");
    //String especialidad = "medicina";
    String servicioUrl = "http://localhost:8080/servicios/webresources/empleado/buscarPorEspecialidad/"+especialidad;
    
    try {

        URL url = new URL(servicioUrl);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("GET");
        conn.setRequestProperty("Accept", "application/json");

        conn.setRequestProperty("Accept-Charset", "UTF-8;q=0.7,*;q=0.7");
        if (conn.getResponseCode() != 200) {
            throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
        }
        BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
        String output;

        while ((output = br.readLine()) != null) {

            JSONParser parser = new JSONParser();
            Object obj = parser.parse(output);
            JSONArray datos = (JSONArray) obj;
            int contador = datos.size();
            //out.print(datos);
            //out.print(contador);
            if (contador == 0) {
                out.print("<strong>No se obtuvo resultados...</strong>");
            } else {

                             
                out.print("<br><div class=\"row\">");

                out.print("<div class=\"col-xs-12\">");
                out.print("<div class=\"input-group\">");
                out.print("<span class=\"input-group-addon\" id=\"sizing-addon2\">Nombre</span>");
                out.print("<select name=\"doctorN\" id=\"doctorN\" class=\"form-control\">");
                out.print("<option value=\"0\" selected=\1\">Seleccione un doctor</option>");
                for (int i = 0; i < contador; i++) {
                    JSONObject js = (JSONObject) datos.get(i);
                    JSONObject msg = (JSONObject) js.get("idPersona");                                
                    out.print("<option value=" + js.get("idEmpleado") + ">" + msg.get("nombres") + "</option>");
                }
                out.print("</select></div></div>");
                
                out.print("<div class=\"col-xs-12\">");
                out.print("<div class=\"input-group\">");
                out.print("<span class=\"input-group-addon\" id=\"sizing-addon2\">Hora Inicio</span>");

                out.print("<input type=\"time\" class=\"form-control\" id=\"hora_inicio\" aria-describedby=\"sizing-addon3\">");
                out.print("</div></div>");
                
                out.print("<div class=\"col-xs-12\">");
                out.print("<div class=\"input-group\">");
                out.print("<span class=\"input-group-addon\" id=\"sizing-addon2\">Hora Fin</span>");

                out.print("<input type=\"time\" class=\"form-control\" id=\"hora_fin\" aria-describedby=\"sizing-addon3\">");
                out.print("</div></div>");
                
                out.print("<div class=\"col-xs-12\">");
                out.print("<div class=\"input-group\">");
                out.print("<span class=\"input-group-addon\" id=\"sizing-addon2\">Detalle</span>");

                out.print("<input type=\"text\" class=\"form-control\" id=\"detalle\" aria-describedby=\"sizing-addon3\">");
                out.print("</div></div>");

                out.print("<div class=\"col-xs-12\">");
                out.print("<button type=\"button\" class=\"btn btn-default btn-lg btn-block\" onclick=\"ingresarHorario();\">Aceptar</button>");
                out.print("</div>");
                out.print("</div>");

            }
        }

        conn.disconnect();

    } catch (MalformedURLException e) {

        out.print("Error con servidor" + e.getMessage());

    } catch (IOException e) {

        out.print("Error con servidor: " + e.getMessage());

    }

%>
