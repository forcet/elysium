<%-- 
    Document   : asignar
    Created on : 26/01/2015, 10:59:10 PM
    Author     : Mario
--%>

<%@page import="org.json.simple.JSONArray"%>
<%@page import="java.io.IOException"%>
<%@page import="java.net.MalformedURLException"%>
<%@page import="org.json.simple.JSONObject"%>
<%@page import="org.json.simple.parser.JSONParser"%>
<%@page import="java.io.InputStreamReader"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="java.net.HttpURLConnection"%>
<%@page import="java.net.URL"%>
<%
    String servicioUrl = "http://localhost:8080/servicios/webresources/persona/buscarPorRol/doctor";
    String[] especialidades = {"traumatologia", "oftalmologia", "otorrinolaringologia", "cardiologia", "nuemologia", "gastroenterologia", "neurologia", "nefrologia", "endocrinologia", "hematologia", "oncologia", "dermatologia", "medicina interna"};
    String valor;
    try {

        URL url = new URL(servicioUrl);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("GET");
        conn.setRequestProperty("Accept", "application/json");

        conn.setRequestProperty("Accept-Charset", "UTF-8;q=0.7,*;q=0.7");
        if (conn.getResponseCode() != 200) {
            throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
        }
        BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
        String output;

        while ((output = br.readLine()) != null) {

            JSONParser parser = new JSONParser();
            Object obj = parser.parse(output);
            JSONArray datos = (JSONArray) obj;
            int contador = datos.size();
            //out.print(datos);
            //out.print(contador);
            if (contador == 0) {
                out.print("<strong>No se obtuvo resultados...</strong>");
            } else {
                             
                out.print("<br><br><br><div class=\"row\">");

                out.print("<div class=\"col-xs-12\">");
                out.print("<div class=\"input-group\">");
                out.print("<span class=\"input-group-addon\" id=\"sizing-addon2\">Nombre</span>");
                out.print("<select name=\"doctor\" id=\"doctor\" class=\"form-control\">");
                out.print("<option value=\"0\" selected=\1\">Seleccione un doctor</option>");
                for (int i = 0; i < contador; i++) {
                    JSONObject js = (JSONObject) datos.get(i);               
                    out.print("<option value=" + js.get("idPersona") + ">" + js.get("nombres") + "</option>");
                }
                out.print("</select></div></div>");
                
                
                out.print("<div class=\"col-xs-12\">");
                out.print("<div class=\"input-group\">");
                out.print("<span class=\"input-group-addon\" id=\"sizing-addon2\">Especialidad</span>");
                out.print("<select name=\"especialidad\" id=\"especialidad\" class=\"form-control\">");
                out.print("<option value=\"0\" selected=\1\">Seleccione una especialidad</option>");
                for (String espe : especialidades) {
                    out.print("<option value=" + espe + ">" + espe + "</option>");
                }

                out.print("</select></div></div><br>");
                out.print("<div class=\"col-xs-12\">");
                out.print("<button type=\"button\" class=\"btn btn-default btn-lg btn-block\" onclick=\"ingresarDatos();\">Aceptar</button>");
                out.print("</div>");
                out.print("</div>");

            }
        }

        conn.disconnect();

    } catch (MalformedURLException e) {

        out.print("Error con servidor" + e.getMessage());

    } catch (IOException e) {

        out.print("Error con servidor: " + e.getMessage());

    }

%>
