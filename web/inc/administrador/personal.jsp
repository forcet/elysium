<%-- 
    Document   : personal
    Created on : 26/01/2015, 10:36:10 PM
    Author     : Mario
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%
    HttpSession sesionActiva = request.getSession();
    String usuario = "";
    String idusuario = "";
    String rol = "";

    if (sesionActiva.getAttribute("idUsuario") == null) {

        response.sendRedirect("../../");
    } else {
        if (sesionActiva.getAttribute("rolUsuario").equals("doctor")) {
            response.sendRedirect("../doctor/");
        } else {
            if (sesionActiva.getAttribute("rolUsuario").equals("paciente")) {
                response.sendRedirect("../paciente/");
            }
        }

        usuario = (String) sesionActiva.getAttribute("nombreUsuario");
        idusuario = (String) sesionActiva.getAttribute("idUsuario");
        rol = (String) sesionActiva.getAttribute("rolUsuario");

    }

    String[] especialidades = {"traumatologia", "oftalmologia", "otorrinolaringologia", "cardiologia", "nuemologia", "gastroenterologia", "neurologia", "nefrologia", "endocrinologia", "hematologia", "oncologia", "dermatologia", "medicina interna"};
    String[] provincias = {"Azuay", "Bolivar", "Carchi", "Cañar", "Chimborazo", "Cotopaxi", "El Oro", "Esmeraldas", "Galápagos", "Guayas", "Imbabura", "Loja", "Los Ríos", "Morona Santiago", "Napo", "Orellana", "Pastaza", "Pichincha", "Santa Elena", "Santo Domingo de los Tsáchilas", "Sucumbíos", "Tungurahua", "Zamora Chinchipe"};
    String[] month = {"Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"};
%>

<!DOCTYPE html>
<html lang="es">
    <head>
        <jsp:include page="../../inc/config/header_common.jsp" flush="true" />
    </head>
    <body>
        <jsp:include page="../../inc/config/header.jsp" flush="true" />  
        <div class="wrapper">
            <div id="content-list"><%--workshop-list --%>
                <article id="w1" class="content content-left"><%--workshop workshop-left--%>
                    <div class="well" style="max-width: 900px; margin: 0 auto 10px;">
                        <div class="row">  

                            <!--                        <div class="col-xs-12"><h2 style="text-align: center">PERFIL</h2></div>-->

                            <div class="col-xs-10 col-md-10 col-lg-10" style="margin: 40px ;">
                                <div class="col-xs-12">
                                    <div class="btn-group btn-group-justified" role="group">
                                        <div class="btn-group" role="group"><h2>PERSONAL</h2></div>
                                        <div class="btn-group" role="group"></div>
                                        <div class="btn-group" role="group">                                           
                                            <button type="button" class="btn btn-success letter" data-toggle="modal" data-target=".bs-example-modal-lg">Nuevo <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> </button>
                                        </div>
                                    </div><br>
                                    <div class="row" style="text-align: center">
                                        <div class="col-xs-2">
                                            <button type="button" class="btn btn-info letter"  onclick="asignarPagina();"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span>  Asignar</button><br>
                                        </div>
                                        <div class="row letter" id="resultados"></div>
                                        <div class="col-xs-10" id="">
                                            
                                        </div>

                                    </div>
                                    <div class="col-xs-12 col-md-12 col-lg-12" style="text-align: right"><p><h3>BIENVENIDO <% out.write(usuario);%></h3></p></div>
                                </div>
                            </div>
                            </article>
                        </div>
                    </div>

                    <div class="modal fade bs-example-modal-lg" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModalLabel">Ingresar nuevo miembro del personal</h4>
                                </div>
                                <div class="modal-body letter">
                                    <div class="row">

                                        <div class="col-xs-12">
                                            <div class="input-group">
                                                <span class="input-group-addon" id="sizing-addon3">Cédula</span>
                                                <input type="text" class="form-control" id="cedula" placeholder="Cédula" aria-describedby="sizing-addon3" maxlength="10" onkeypress="return soloNumero(event)">
                                            </div>
                                        </div>

                                        <div class="col-xs-12">
                                            <div class="input-group">
                                                <span class="input-group-addon" id="sizing-addon3">Primer Nombre</span>
                                                <input type="text" class="form-control" id="primerNombre" placeholder="Primer Nombre" aria-describedby="sizing-addon3" onkeypress="return soloLetras(event)">
                                            </div>
                                        </div>

                                        <div class="col-xs-12">
                                            <div class="input-group">
                                                <span class="input-group-addon" id="sizing-addon3">Segundo Nombre</span>
                                                <input type="text" class="form-control" id="segundoNombre" placeholder="Segundo Nombre" aria-describedby="sizing-addon3" onkeypress="return soloLetras(event)">
                                            </div>
                                        </div>
                                        <div class="col-xs-12">
                                            <div class="input-group">
                                                <span class="input-group-addon" id="sizing-addon3">Apellido Paterno</span>
                                                <input type="text" class="form-control" id="apellidoP" placeholder="Apellido Paterno" aria-describedby="sizing-addon3" onkeypress="return soloLetras(event)">
                                            </div>
                                        </div>

                                        <div class="col-xs-12">
                                            <div class="input-group">
                                                <span class="input-group-addon" id="sizing-addon3">Apellido Materno</span>
                                                <input type="text" class="form-control" id="apellidoM" placeholder="Apellido Materno" aria-describedby="sizing-addon3" onkeypress="return soloLetras(event)">
                                            </div>
                                        </div>

                                        <div class="col-xs-12">
                                            <div class="input-group">
                                                <span class="input-group-addon" id="sizing-addon3">Sexo</span>
                                                <select name="sexo" id="sexo" class="form-control" onchange="">
                                                    <option>-- Seleccione una opción --</option>
                                                    <option value="Masculino">Masculino</option>
                                                    <option value="Femenino">Femenino</option>
                                                    <option value="Indefinido">Indefinido</option>
                                                </select>                                       
                                            </div>
                                        </div>
                                        <div class="col-xs-12">
                                            <div class="input-group">
                                                <span class="input-group-addon" id="sizing-addon3">Tipo de Sangre</span>
                                                <input type="text" class="form-control" id="sangre" placeholder="Tipo de Sangre" aria-describedby="sizing-addon3">
                                            </div>
                                        </div>
                                        <div class="col-xs-12">
                                            <div class="input-group">
                                                <span class="input-group-addon" id="sizing-addon3">Cumpleaños</span>
                                                <div class="btn-group btn-group-justified" role="group" aria-label="...">

                                                    <select name="meses" id="meses" class="form-control">
                                                        <option value="0" selected="1">Mes</option>
                                                        <%
                                                            int j = 1;
                                                            for (String months : month) {
                                                                out.print("<option value=" + j + ">" + months + "</option>");
                                                                j++;
                                                            }
                                                        %>
                                                    </select>
                                                    <select name="dia" id="dia" class="form-control">
                                                        <option value="0" selected="1">Día</option>
                                                        <%
                                                            for (int x = 1; x <= 31; x++) {
                                                                out.print("<option value=" + x + ">" + x + "</option>");
                                                            }
                                                        %>                                                    
                                                    </select>
                                                    <select name="anio" id="anio" class="form-control">
                                                        <option value="0" selected="1">Año</option>
                                                        <%
                                                            for (int an = 2015; an >= 1905; an--) {
                                                                out.print("<option value=" + an + ">" + an + "</option>");
                                                            }
                                                        %>
                                                    </select>

                                                </div>                                            
                                            </div>
                                        </div>
                                        <form name="form">
                                            <div class="col-xs-12">
                                                <div class="input-group">
                                                    <span class="input-group-addon" id="sizing-addon3">Provincia</span>
                                                    <select name="select" class="form-control" id="provincias" onchange="slctryole(this, this.form.cantones)">
                                                        <option></option>
                                                        <%
                                                            for (String prov : provincias) {
                                                                out.write("<option value='" + prov.replace(" ", "_").toLowerCase() + "'>" + prov + "</option>");
                                                            }
                                                        %>

                                                    </select>

                                                </div>
                                            </div>
                                            <div class="col-xs-12">
                                                <div class="input-group">
                                                    <span class="input-group-addon" id="sizing-addon3">Cantones</span>
                                                    <select name="cantones" id="cantones" class="form-control" onchange="">
                                                        <option></option>                                                
                                                    </select>                                       
                                                </div>
                                            </div>
                                        </form>
                                        <div class="col-xs-12">
                                            <div class="input-group">
                                                <span class="input-group-addon" id="sizing-addon3">Parroquia</span>
                                                <input type="correo" class="form-control" id="parroquia" placeholder="Parroquia" aria-describedby="sizing-addon3" onkeypress="return caractNumEs(event)">
                                            </div>
                                        </div>
                                        <div class="col-xs-12">
                                            <div class="input-group">
                                                <span class="input-group-addon" id="sizing-addon3">Barrio</span>
                                                <input type="text" class="form-control" id="barrio" placeholder="Barrio" aria-describedby="sizing-addon3" onkeypress="return caractNumEs(event)">
                                            </div>
                                        </div>
                                        <div class="col-xs-12">
                                            <div class="input-group">
                                                <span class="input-group-addon" id="sizing-addon3">Correo</span>
                                                <input type="correo" class="form-control" id="correo" placeholder="Correo" aria-describedby="sizing-addon3" onkeypress="return correo(event)">
                                            </div>
                                        </div>
                                        <div class="col-xs-12">
                                            <div class="input-group">
                                                <span class="input-group-addon" id="sizing-addon3">Teléfono</span>
                                                <input type="text" class="form-control" id="telefono" placeholder="Teléfono" aria-describedby="sizing-addon3" onkeypress="return soloNumero(event)">
                                            </div>
                                        </div>
                                        <div class="col-xs-12">
                                            <div class="input-group">
                                                <span class="input-group-addon" id="sizing-addon3">Usuario</span>
                                                <input type="text" class="form-control" id="usuario" placeholder="Usuario" aria-describedby="sizing-addon3" onkeypress="return caractNum(event)">
                                            </div>
                                        </div>
                                        <div class="col-xs-12">
                                            <div class="input-group">
                                                <span class="input-group-addon" id="sizing-addon3">Contraseña</span>
                                                <input type="password" class="form-control" id="clave" placeholder="Contraseña" aria-describedby="sizing-addon3">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                                    <!--                        <button type="button" id="enviar" class="btn btn-default">Registrarse</button>-->
                                    <button type="button" id="datosPa" onclick="ingresarNuevo();" class="btn btn-primary">Guardar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <script src="../../js/elysium/provinicias.js" type="text/javascript"></script>
                    <jsp:include page="../../inc/config/footer.jsp" flush="true" />
                    <jsp:include page="../../inc/config/footer_common.jsp" flush="true" />
                    <script src="../../js/md5/md5.min.js" type="text/javascript"></script>
                    <script src="../../js/elysium/configuraciones.js" type="text/javascript"></script>
                    <script src="../../js/elysium/adminstrador.js" type="text/javascript"></script>
                    </body>
                    </html>
