<%-- 
    Document   : horariro
    Created on : 26/01/2015, 08:56:29 PM
    Author     : Mario
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%
    HttpSession sesionActiva = request.getSession();
    String usuario = "";
    String idusuario = "";
    String rol = "";

    if (sesionActiva.getAttribute("idUsuario") == null) {

        response.sendRedirect("../../");
    } else {
        if (sesionActiva.getAttribute("rolUsuario").equals("doctor")) {
            response.sendRedirect("../doctor/");
        } else {
            if (sesionActiva.getAttribute("rolUsuario").equals("paciente")) {
                response.sendRedirect("../paciente/");
            }
        }

        usuario = (String) sesionActiva.getAttribute("nombreUsuario");
        idusuario = (String) sesionActiva.getAttribute("idUsuario");
        rol = (String) sesionActiva.getAttribute("rolUsuario");

    }

    String[] especialidades = {"traumatologia", "oftalmologia", "otorrinolaringologia", "cardiologia", "nuemologia", "gastroenterologia", "neurologia", "nefrologia", "endocrinologia", "hematologia", "oncologia", "dermatologia", "medicina"};
%>

<!DOCTYPE html>
<html lang="es">
    <head>
        <jsp:include page="../../inc/config/header_common.jsp" flush="true" />
    </head>
    <body>
        <jsp:include page="../../inc/config/header.jsp" flush="true" />  
        <div class="wrapper">
            <div id="content-list"><%--workshop-list --%>
                <article id="w1" class="content content-left"><%--workshop workshop-left--%>
                    <div class="well" style="max-width: 900px; margin: 0 auto 10px;">
                        <div class="row">  

                            <!--                        <div class="col-xs-12"><h2 style="text-align: center">PERFIL</h2></div>-->

                            <div class="col-xs-10 col-md-10 col-lg-10" style="margin: 40px ;">
                                <div class="col-xs-12">
                                    <div class="btn-group btn-group-justified" role="group">
                                        <div class="btn-group" role="group"><span class="glyphicon glyphicon-th-list" aria-hidden="true"></span></div>
                                        <div class="btn-group" role="group"></div>
                                        <div class="btn-group" role="group"><h2>HORARIOS</h2></div>
                                    </div><br>
                                </div>
                                <div class="row" style="text-align: center">
                                    <div class="col-xs-10 letter">
                                        <div class="input-group">
                                            <span class="input-group-addon" id="sizing-addon2">Especialidades</span>
                                            <select name="doctor" id="doctor" class="form-control" onchange="asignarHorario()">
                                                <option value="0" selected=1">Seleccione una especialidad</option>
                                                <%
                                                    for (String spec : especialidades) {
                                                        out.print("<option value=" + spec + " >" + spec + "</option>");
                                                    }
                                                %>
                                            </select>
                                        </div>
                                            
                                            <div id="resultados"></div>
                                    </div>
                                    
                                </div>

                            </div>
                            <div class="col-xs-12 col-md-12 col-lg-12" style="text-align: right"><p><h3>BIENVENIDO <% out.write(usuario);%></h3></p></div>
                        </div>
                    </div>
                </article>
            </div>
        </div>


        <jsp:include page="../../inc/config/footer.jsp" flush="true" />
        <jsp:include page="../../inc/config/footer_common.jsp" flush="true" />
        <script src="../../js/elysium/configuraciones.js" type="text/javascript"></script>
        <script src="../../js/elysium/adminstrador.js" type="text/javascript"></script>
    </body>
</html>
