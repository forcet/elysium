/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function Mayus(cadena) {
    var palabras;
    var nuevaCadena = "";
    var len;
    palabras = cadena.split(" ");
    len = palabras.length;
    for (var i = 0; i < len; i++) {
        if (i != (len - 1)) {
            nuevaCadena = nuevaCadena + primeraMayus(palabras[i]) + " ";
        }
        else {
            nuevaCadena = nuevaCadena + primeraMayus(palabras[i]);
        }
    }
    return nuevaCadena;
}
function primeraMayus(primera) {
    return primera.substr(0, 1).toUpperCase() + primera.substr(1, primera.length).toLowerCase();
}

function correo(e) { //Caracteres y numeros
    tecla = (document.all) ? e.keyCode : e.which; 
    if (tecla==8) return true; 
        patron =/[a-zA-Z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF\@\w\.]/; 
        te = String.fromCharCode(tecla); 
        return patron.test(te); 
}

function caractNum(e) { //Caracteres y numeros
    tecla = (document.all) ? e.keyCode : e.which; 
    if (tecla==8) return true; 
        patron =/[a-zA-Z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF\w\.]/; 
        te = String.fromCharCode(tecla); 
        return patron.test(te); 
}
function caractNumEs(e) { //Caracteres y numeros
    tecla = (document.all) ? e.keyCode : e.which; 
    if (tecla==8) return true; 
        patron =/[a-zA-Z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF\w\.\,\" "]/; 
        te = String.fromCharCode(tecla); 
        return patron.test(te); 
}

function soloLetras(e) { // Solo letras
    tecla = (document.all) ? e.keyCode : e.which; // 
    if (tecla==8) return true; // 
        patron =/[a-zA-Z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF\""\ ]/; // 
        te = String.fromCharCode(tecla); // 
        return patron.test(te); // 
}

function soloNumero(e) { // Solo numeros
    tecla = (document.all)?e.keyCode:e.which;
    if (tecla==8) return true;
    patron = /\d/;
    te = String.fromCharCode(tecla);
    return patron.test(te); 
}

function get(url,combo, valor, texto){
    $.ajax({
        url: url,
        type: 'get',
        dataType:'json',
        success: function (data) {
            llenarCombo(combo, valor, texto, data);
        },
        error: function(e, msg){ 
            console.log(e);
        }
    });
}

function llenarCombo(combo, valor, texto, items)
{
    cmb = document.getElementById(combo);
    cmb.length = 1;
    for (var i = 0; i<items.length; i++){
        var opt = document.createElement('option');
        opt.value = items[i][valor];
        opt.innerHTML = items[i][texto];
        cmb.appendChild(opt);
    }
}

function devolverValorOTextoCombo(combo, devolver)
{
    var lista = document.getElementById(combo);
    if(devolver == 'valor'){
        return parseInt(lista.options[lista.selectedIndex].value);
    }if(devolver == 'texto'){
        return lista.options[lista.selectedIndex].text;
    }
}