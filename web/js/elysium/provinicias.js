/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function slctr(texto,valor){
	this.texto = texto;
	this.valor = valor;
}

var azuay =new Array();
	azuay[0] = new slctr('- -Seleccione una opción- -');
	azuay[1] = new slctr("Chordeleg",'Chordeleg');
        azuay[2] = new slctr("Cuenca",'Cuenca');
        azuay[3] = new slctr("El Pan",'El Pan');
        azuay[4] = new slctr("Girón",'Girón');
        azuay[5] = new slctr("Guachapala",'Guachapala');
        azuay[6] = new slctr("Gualaceo",'Gualaceo');
        azuay[7] = new slctr("Nabón",'Nabón');
        azuay[8] = new slctr("Oña",'Oña');
        azuay[9] = new slctr("Paute",'Paute');
        azuay[10] = new slctr("Ponce Enriquez",'Ponce Enriquez');
        azuay[11] = new slctr("Pucará",'Pucará');
        azuay[12] = new slctr("San Fernando",'San Fernando');
        azuay[13] = new slctr("Santa Isabel",'Santa Isabel');
        azuay[14] = new slctr("Sevilla de Oro",'Sevilla de Oro');
        azuay[15] = new slctr("Sígsig",'Sígsig');
        
var bolivar =new Array();
	bolivar[0] = new slctr('- -Seleccione una opción- -');
	bolivar[1] = new slctr("Caluma",'Caluma');
        bolivar[2] = new slctr("Chillanes",'Chillanes');
        bolivar[3] = new slctr("Chimbo",'Chimbo');
        bolivar[4] = new slctr("Echeandía",'Echeandía');
        bolivar[5] = new slctr("Guaranda",'Guaranda');
        bolivar[6] = new slctr("Las Naves",'Las Naves');
        bolivar[7] = new slctr("San Miguel",'San Miguel');
        
var cañar =new Array();
	cañar[0] = new slctr('- -Seleccione una opción- -');
	cañar[1] = new slctr("Azogues",'Azogues');
        cañar[2] = new slctr("Biblián",'Biblián');
        cañar[3] = new slctr("Cañar",'Cañar');
        cañar[4] = new slctr("Déleg",'Déleg');
        cañar[5] = new slctr("El Tambo",'El Tambo');
        cañar[6] = new slctr("La Troncal",'La Troncal');
        cañar[7] = new slctr("Suscal",'Suscal');
        
var carchi =new Array();
	carchi[0] = new slctr('- -Seleccione una opción- -');
	carchi[1] = new slctr("Bolívar",'Bolívar');
        carchi[2] = new slctr("Espejo",'Espejo');
        carchi[3] = new slctr("Mira",'Mira');
        carchi[4] = new slctr("Montúfar",'Montúfar');
        carchi[5] = new slctr("San Pedro de Huaca",'San Pedro de Huaca');
        carchi[6] = new slctr("Tulcán",'Tulcán');
        
var chimborazo =new Array();
	chimborazo[0] = new slctr('- -Seleccione una opción- -');
	chimborazo[1] = new slctr("Alausí",'Alausí');
        chimborazo[2] = new slctr("Chambo",'Chambo');
        chimborazo[3] = new slctr("Chunchi",'Chunchi');
        chimborazo[4] = new slctr("Colta",'Colta');
        chimborazo[5] = new slctr("Cumandá",'Cumandá');
        chimborazo[6] = new slctr("Guamote",'Guamote');
        chimborazo[7] = new slctr("Guano",'Guano');
        chimborazo[8] = new slctr("Pallatanga",'Pallatanga');
        chimborazo[9] = new slctr("Penipe",'Penipe');
        chimborazo[10] = new slctr("Riobamba",'Riobamba');

var cotopaxi =new Array();
	cotopaxi[0] = new slctr('- -Seleccione una opción- -');
	cotopaxi[1] = new slctr("La Maná",'La Maná');
        cotopaxi[2] = new slctr("Latacunga",'Latacunga');
        cotopaxi[3] = new slctr("Pangua",'Pangua');
        cotopaxi[4] = new slctr("Pujilí",'Pujilí');
        cotopaxi[5] = new slctr("Salcedo",'Salcedo');
        cotopaxi[6] = new slctr("Saquisilí",'Saquisilí');
        cotopaxi[7] = new slctr("Sigchos",'Sigchos');

var el_oro =new Array();
	el_oro[0] = new slctr('- -Seleccione una opción- -');
	el_oro[1] = new slctr("Arenillas",'Arenillas');
        el_oro[2] = new slctr("Atahualpa",'Atahualpa');
        el_oro[3] = new slctr("Balsas",'Balsas');
        el_oro[4] = new slctr("Chilla",'Chilla');
        el_oro[5] = new slctr("El Guabo",'El Guabo');
        el_oro[6] = new slctr("Huaquillas",'Huaquillas');
        el_oro[7] = new slctr("Las Lajas",'Las Lajas');
        el_oro[8] = new slctr("Machala",'Machala');
        el_oro[9] = new slctr("Marcabelí",'Marcabelí');
        el_oro[10] = new slctr("Pasaje",'Pasaje');
        el_oro[11] = new slctr("Piñas",'Piñas');
        el_oro[12] = new slctr("Portovelo",'Portovelo');
        el_oro[13] = new slctr("Santa Rosa",'Santa Rosa');
        el_oro[14] = new slctr("Zaruma",'Zaruma');

var esmeraldas =new Array();
	esmeraldas[0] = new slctr('- -Seleccione una opción- -');
	esmeraldas[1] = new slctr("Esmeraldas",'Esmeraldas');
        esmeraldas[2] = new slctr("Eloy Alfaro",'Eloy Alfaro');
        esmeraldas[3] = new slctr("Muisne",'Muisne');
        esmeraldas[4] = new slctr("Quinindé",'Quinindé');
        esmeraldas[5] = new slctr("San Lorenzo",'San Lorenzo');
        esmeraldas[6] = new slctr("Atacames",'Atacames');
        esmeraldas[7] = new slctr("Río Verde",'Río Verde');

var galápagos =new Array();
	galápagos[0] = new slctr('- -Seleccione una opción- -');
	galápagos[1] = new slctr("Isabela",'Isabela');
        galápagos[2] = new slctr("San Cristóbal",'San Cristóbal');
        galápagos[3] = new slctr("Santa Cruz",'Santa Cruz');

var guayas =new Array();
	guayas[0] = new slctr('- -Seleccione una opción- -');
	guayas[1] = new slctr("Guayaquil",'Guayaquil');
        guayas[2] = new slctr("Alfredo Baquerizo Moreno",'Alfredo Baquerizo Moreno');
        guayas[3] = new slctr("Balao",'Balao');
        guayas[4] = new slctr("Balzar",'Balzar');
        guayas[5] = new slctr("Colimes",'Colimes');
        guayas[6] = new slctr("Daule",'Daule');
        guayas[7] = new slctr("El Empalme",'El Empalme');
        guayas[8] = new slctr("El Triunfo",'El Triunfo');
        guayas[9] = new slctr("Eloy Alfaro",'Eloy Alfaro');
        guayas[10] = new slctr("General Antonio Elizalde",'General Antonio Elizalde');
        guayas[11] = new slctr("General Villamil",'General Villamil');
        guayas[12] = new slctr("Isidro Ayora",'Isidro Ayora');
        guayas[13] = new slctr("Lomas de Sargentillo",'Lomas de Sargentillo');
        guayas[14] = new slctr("Marcelino Maridueña",'Marcelino Maridueña');
        guayas[15] = new slctr("Milagro",'Milagro');
        guayas[16] = new slctr("Naranjal",'Naranjal');
        guayas[17] = new slctr("Naranjito",'Naranjito');
        guayas[18] = new slctr("Nobol",'Nobol');
        guayas[19] = new slctr("Palestina",'Palestina');
        guayas[20] = new slctr("Pedro Carbo",'Pedro Carbo');
        guayas[21] = new slctr("Salitre",'Salitre');
        guayas[22] = new slctr("Samborondón",'Samborondón');
        guayas[23] = new slctr("Santa Lucía",'Santa Lucía');
        guayas[24] = new slctr("Simón Bolívar",'Simón Bolívar');
        guayas[25] = new slctr("Yaguachi",'Yaguachi');

var imbabura =new Array();
	imbabura[0] = new slctr('- -Seleccione una opción- -');
	imbabura[1] = new slctr("Antonio Ante",'Antonio Ante');
        imbabura[2] = new slctr("Cotacachi",'Cotacachi');
        imbabura[3] = new slctr("Ibarra",'Ibarra');
        imbabura[4] = new slctr("Otavalo",'Otavalo');
        imbabura[5] = new slctr("Pimampiro",'Pimampiro');
        imbabura[6] = new slctr("San Miguel de Urcuquí",'San Miguel de Urcuquí');

var loja =new Array();
	loja[0] = new slctr('- -Seleccione una opción- -');
	loja[1] = new slctr("Calvas",'Calvas');
        loja[2] = new slctr("Catamayo",'Catamayo');
        loja[3] = new slctr("Celica",'Celica');
        loja[4] = new slctr("Chaguarpamba",'Chaguarpamba');
        loja[5] = new slctr("Espíndola",'Espíndola');
        loja[6] = new slctr("Gonzanamá",'Gonzanamá');
        loja[7] = new slctr("Loja",'Loja');
        loja[8] = new slctr("Macará",'Macará');
        loja[9] = new slctr("Olmedo",'Olmedo');
        loja[10] = new slctr("Paltas",'Paltas');
        loja[11] = new slctr("Pindal",'Pindal');
        loja[12] = new slctr("Puyango",'Puyango');
        loja[13] = new slctr("Quilanga",'Quilanga');
        loja[14] = new slctr("Saraguro",'Saraguro');
        loja[15] = new slctr("Sozoranga",'Sozoranga');
        loja[16] = new slctr("Zapotillo",'Zapotillo');

var los_ríos =new Array();
	los_ríos[0] = new slctr('- -Seleccione una opción- -');
	los_ríos[1] = new slctr("Baba",'Baba');
        los_ríos[2] = new slctr("Babahoyo",'Babahoyo');
        los_ríos[3] = new slctr("Buena Fe",'Buena Fe');
        los_ríos[4] = new slctr("Mocache",'Mocache');
        los_ríos[5] = new slctr("Montalvo",'Montalvo');
        los_ríos[6] = new slctr("Palenque",'Palenque');
        los_ríos[7] = new slctr("Puebloviejo",'Puebloviejo');
        los_ríos[8] = new slctr("Quevedo",'Quevedo');
        los_ríos[9] = new slctr("Quinsaloma",'Quinsaloma');
        los_ríos[10] = new slctr("Urdaneta",'Urdaneta');
        los_ríos[11] = new slctr("Valencia",'Valencia');
        los_ríos[12] = new slctr("Ventanas",'Ventanas');
        los_ríos[13] = new slctr("Vinces",'Vinces');

var manabí =new Array();
	manabí[0] = new slctr('- -Seleccione una opción- -');
	manabí[1] = new slctr("Bolívar",'Bolívar');
        manabí[2] = new slctr("Chone",'Chone');
        manabí[3] = new slctr("El Carmen",'El Carmen');
        manabí[4] = new slctr("Flavio Alfaro",'Flavio Alfaro');
        manabí[5] = new slctr("Jama",'Jama');
        manabí[6] = new slctr("Jaramijó",'Jaramijó');
        manabí[7] = new slctr("Jipijapa",'Jipijapa');
        manabí[8] = new slctr("Junín",'Junín');
        manabí[9] = new slctr("Manta",'Manta');
        manabí[10] = new slctr("Montecristi",'Montecristi');
        manabí[11] = new slctr("Olmedo",'Olmedo');
        manabí[12] = new slctr("Paján",'Paján');
        manabí[13] = new slctr("Pedernales",'Pedernales');
        manabí[14] = new slctr("Pichincha",'Pichincha');
        manabí[15] = new slctr("Portoviejo",'Portoviejo');
        manabí[16] = new slctr("Puerto López",'Puerto López');
        manabí[17] = new slctr("Rocafuerte",'Rocafuerte');
        manabí[18] = new slctr("San Vicente",'San Vicente');
        manabí[19] = new slctr("Santa Ana",'Santa Ana');
        manabí[20] = new slctr("Sucre",'Sucre');
        manabí[21] = new slctr("Tosagua",'Tosagua');
        manabí[22] = new slctr("Veinticuatro de mayo",'Veinticuatro de mayo');

var morona_santiago =new Array();
	morona_santiago[0] = new slctr('- -Seleccione una opción- -');
	morona_santiago[1] = new slctr("Gualaquiza",'Gualaquiza');
        morona_santiago[2] = new slctr("Huamboya",'Huamboya');
        morona_santiago[3] = new slctr("Limón Indanza",'Limón Indanza');
        morona_santiago[4] = new slctr("Logroño",'Logroño');
        morona_santiago[5] = new slctr("Morona",'Morona');
        morona_santiago[6] = new slctr("Pablo Sexto",'Pablo Sexto');
        morona_santiago[7] = new slctr("Palora",'Palora');
        morona_santiago[8] = new slctr("San Juan Bosco",'San Juan Bosco');
        morona_santiago[9] = new slctr("Santiago",'Santiago');
        morona_santiago[10] = new slctr("Sucúa",'Sucúa');
        morona_santiago[11] = new slctr("Taisha",'Taisha');
        morona_santiago[12] = new slctr("Tiwintza",'Tiwintza');

var napo =new Array();
	napo[0] = new slctr('- -Seleccione una opción- -');
	napo[1] = new slctr("Archidona",'Archidona');
        napo[2] = new slctr("Carlos Julio Arosemena Tola",'Carlos Julio Arosemena Tola');
        napo[3] = new slctr("El Chaco",'El Chaco');
        napo[4] = new slctr("Quijos",'Quijos');
        napo[5] = new slctr("Tena",'Tena');

var orellana =new Array();
	orellana[0] = new slctr('- -Seleccione una opción- -');
	orellana[1] = new slctr("Aguarico",'Aguarico');
        orellana[2] = new slctr("Francisco de Orellana",'Francisco de Orellana');
        orellana[3] = new slctr("La Joya de los Sachas",'La Joya de los Sachas');
        orellana[4] = new slctr("Loreto",'Loreto');

var pastaza =new Array();
	pastaza[0] = new slctr('- -Seleccione una opción- -');
	pastaza[1] = new slctr("Arajuno",'Arajuno');
        pastaza[2] = new slctr("Mera",'Mera');
        pastaza[3] = new slctr("Pastaza",'Pastaza');
        pastaza[4] = new slctr("Santa Clara",'Santa Clara');

var pichincha =new Array();
	pichincha[0] = new slctr('- -Seleccione una opción- -');
	pichincha[1] = new slctr("Cayambe",'Cayambe');
        pichincha[2] = new slctr("Mejía",'Mejía');
        pichincha[3] = new slctr("Pedro Moncayo",'Pedro Moncayo');
        pichincha[4] = new slctr("Pedro Vicente Maldonado",'Pedro Vicente Maldonado');
        pichincha[5] = new slctr("Puerto Quito",'Puerto Quito');
        pichincha[6] = new slctr("Distrito Metropolitano de Quito",'Distrito Metropolitano de Quito');
        pichincha[7] = new slctr("Rumiñahui",'Rumiñahui');
        pichincha[8] = new slctr("San Miguel de Los Bancos",'San Miguel de Los Bancos');
        
var santa_elena =new Array();
	santa_elena[0] = new slctr('- -Seleccione una opción- -');
	santa_elena[1] = new slctr("La Libertad",'La Libertad');
        santa_elena[2] = new slctr("Salinas",'Salinas');
        santa_elena[3] = new slctr("Santa Elena",'Santa Elena');

var santo_domingo_de_los_tsáchilas =new Array();
	santo_domingo_de_los_tsáchilas[0] = new slctr('- -Seleccione una opción- -');
	santo_domingo_de_los_tsáchilas[1] = new slctr("Santo Domingo",'Santo Domingo');
        santo_domingo_de_los_tsáchilas[2] = new slctr("La Concordia",'La Concordia');
        

var sucumbíos =new Array();
	sucumbíos[0] = new slctr('- -Seleccione una opción- -');
	sucumbíos[1] = new slctr("Cascales",'Cascales');
        sucumbíos[2] = new slctr("Cuyabeno",'Cuyabeno');
        sucumbíos[3] = new slctr("Gonzalo Pizarro",'Gonzalo Pizarro');
        sucumbíos[4] = new slctr("Lago Agrio",'Lago Agrio');
        sucumbíos[5] = new slctr("Putumayo",'Putumayo');
        sucumbíos[6] = new slctr("Shushufindi",'Shushufindi');
        sucumbíos[7] = new slctr("Sucumbíos",'Sucumbíos');

var tungurahua =new Array();
	tungurahua[0] = new slctr('- -Seleccione una opción- -');
	tungurahua[1] = new slctr("Ambato",'Ambato');
        tungurahua[2] = new slctr("Baños",'Baños');
        tungurahua[3] = new slctr("Cevallos",'Cevallos');
        tungurahua[4] = new slctr("Mocha",'Mocha');
        tungurahua[5] = new slctr("Patate",'Patate');
        tungurahua[6] = new slctr("Pelileo",'Pelileo');
        tungurahua[7] = new slctr("Píllaro",'Píllaro');
        tungurahua[8] = new slctr("Quero",'Quero');
        tungurahua[9] = new slctr("Tisaleo",'Tisaleo');

var zamora_chinchipe =new Array();
	zamora_chinchipe[0] = new slctr('- -Seleccione una opción- -');
	zamora_chinchipe[1] = new slctr("Centinela del Cóndor",'Centinela del Cóndor');
        zamora_chinchipe[2] = new slctr("Chinchipe",'Chinchipe');
        zamora_chinchipe[3] = new slctr("El Pangui",'El Pangui');
        zamora_chinchipe[4] = new slctr("Nangaritza",'Nangaritza');
        zamora_chinchipe[5] = new slctr("Palanda",'Palanda');
        zamora_chinchipe[6] = new slctr("Paquisha",'Paquisha');
        zamora_chinchipe[7] = new slctr("Yacuambi",'Yacuambi');
        zamora_chinchipe[8] = new slctr("Yantzaza",'Yantzaza');
        zamora_chinchipe[9] = new slctr("Zamora",'Zamora');
        
function slctryole(cual,donde){
	if(cual.selectedIndex != 0){
		donde.length=0;
		cual = eval(cual.value);
		for(m=0;m<cual.length;m++){
			var nuevaOpcion = new Option(cual[m].texto);
			donde.options[m] = nuevaOpcion;
			if(cual[m].valor != null){
				donde.options[m].value = cual[m].valor;
			}
			else{
				donde.options[m].value = cual[m].texto;
			}
		}
	}
}