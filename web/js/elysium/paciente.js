/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var server = "http://localhost:8080/servicios/webresources/";

$('#datosPa').click(function (event) {
    event.preventDefault();
    var cedula = $('#cedula').val();
    var primerNombre = $('#primerNombre').val();
    var segundoNombre = $('#segundoNombre').val();
    var apellidoP = $('#apellidoP').val();
    var apellidoM = $('#apellidoM').val();
    var provincia = $('#provincias').val();
    var canton = $('#cantones').val();
    var parroquia = $('#parroquia').val();
    var barrio = $('#barrio').val();
    var clave = $('#clave').val();
    //clave = md5(clave);
    if(clave == ""){
        clave = $('#passw').val();
    }else{
        
        clave = $('#clave').val();
        clave = md5(clave);
    }
    var correo = $('#correo').val();
    var usuario = $('#usuario').val();
    var sexo = $('#sexo').val();
    var telefono = $('#telefono').val();
    var sangre = $('#sangre').val();
    var dia = $('#dia').val();
    if(dia.length==1){
        dia = "0"+dia;
    }else{
        dia = dia;
    }
    var anio = $('#anio').val();
    var mes = $('#meses').val();
    if(mes.length==1){
        mes = "0"+mes;
    }else{
        mes = mes;
    }
    var idPaciente = $('#idPaciente').val();
    
    var fecha = anio + "-" + mes + "-" + dia +"T16:24:42-05:00";
    provincia = Mayus(provincia.replace("_", " "));
   // 
    alert(clave);
    var data = {"apellidos": apellidoP + " " + apellidoM, "barrio": barrio, "canton": canton, "cedula": cedula, "contrasena": clave, "correo": correo, "fechaNacimiento": fecha, "idPersona": idPaciente, "nombres": primerNombre + " " + segundoNombre, "parroquia": parroquia, "provincia": provincia, "rol": "paciente", "sexo": sexo, "telefono": telefono, "tipoSangre": sangre, "usuario": usuario};
    
    if (cedula != "" && primerNombre != "" && apellidoP != "" && usuario != "" ) {
       actualizaDatosPaciente(data,idPaciente);
    } else {
        alert("Llene todos los campos");
    }
});

function actualizaDatosPaciente(datos,id){
    var url = server+"persona/"+id;
    //var data = {"apellidos": apellidoP + " " + apellidoM, "barrio": barrio, "canton": canton, "cedula": cedula, "contrasena": clave, "correo": correo, "fechaNaci": fecha, "idPersona": 3, "nombres": primerNombre + " " + segundoNombre, "parraquia": parroquia, "provincia": provincia, "rol": "paciente", "sexo": sexo, "telefono": telefono, "tipoSangre": sangre, "usuario": usuario};
    var data = datos;
    console.log(data);
    $.ajax({
        type: 'PUT',
        contentType: 'application/json',
        url: url,
        dataType: 'json',
        data: JSON.stringify(data),
        success: function (data, textStatus, jqXHR) {
            alert("Datos Actualizados Correctamente");
           location.href = "javascript:location.reload()";           
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert('Ocurrio un error - Reintente Nuevamente ' + jqXHR + textStatus + errorThrown);
        }
    });
}

