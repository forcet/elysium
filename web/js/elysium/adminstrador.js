/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var server = "http://localhost:8080/servicios/webresources/";
function ejemplo(){
    var cedula = $('#cedula').val();
    alert(cedula);
}
function ingresarNuevo(){
    
    var cedula = $('#cedula').val();
    var primerNombre = $('#primerNombre').val();
    var segundoNombre = $('#segundoNombre').val();
    var apellidoP = $('#apellidoP').val();
    var apellidoM = $('#apellidoM').val();
    var provincia = $('#provincias').val();
    var canton = $('#cantones').val();
    var parroquia = $('#parroquia').val();
    var barrio = $('#barrio').val();
    var clave = $('#clave').val();
    var correo = $('#correo').val();
    var usuario = $('#usuario').val();
    var sexo = $('#sexo').val();
    var telefono = $('#telefono').val();
    var sangre = $('#sangre').val();
    var dia = $('#dia').val();
    if(dia.length==1){
        dia = "0"+dia;
    }else{
        dia = dia;
    }
    var anio = $('#anio').val();
    var mes = $('#meses').val();
    if(mes.length==1){
        mes = "0"+mes;
    }else{
        mes = mes;
    }
    var fecha = anio + "-" + mes + "-" + dia + "T19:41:36-05:00";
    provincia = Mayus(provincia.replace("_", " "));
    clave = md5(clave);
    //alert(fecha);
    var data = {"apellidos": apellidoP + " " + apellidoM, "barrio": barrio, "canton": canton, "cedula": cedula, "contrasena": clave, "correo": correo, "fechaNacimiento": fecha, "idPersona": 1, "nombres": primerNombre + " " + segundoNombre, "parroquia": parroquia, "provincia": provincia, "rol": "doctor", "sexo": sexo, "telefono": telefono, "tipoSangre": sangre, "usuario": usuario};
    
    if (cedula != "" && primerNombre != "" && apellidoP != "" && usuario != "" && clave != "") {
        comprobarRegistro(cedula, "doctor",data);
        alert("hola");
        console.log(data);
    } else {
        alert("Llene todos los campos");
    }
}


function asignarPagina(objeto) {
    
    $.ajax({
        contentType: "application/x-www-form-urlencoded",
        type: "POST",
        url: "cargas/asignar.jsp",
        async: true,
        success: function (respuesta) {
            //alert(respuesta)
            $("#resultados").html(respuesta);
        },
        error: function (xhr, ajaxOptions, thrownError) {
            //$("#contenido").html('<h1>ERROR</h1>');
            alert(xhr.status + " " + thrownError);
        }
    });
}

function asignarHorario(objeto) {
    var doctor = $('#doctor').val();
    $.ajax({
        contentType: "application/x-www-form-urlencoded",
        type: "POST",
        url: "cargas/asignarHorarios.jsp?esp="+doctor,
        async: true,
        data: "doctor="+doctor,
        success: function (respuesta) {
            //alert(respuesta)
            $("#resultados").html(respuesta);
        },
        error: function (xhr, ajaxOptions, thrownError) {
            //$("#contenido").html('<h1>ERROR</h1>');
            alert(xhr.status + " " + thrownError);
        }
    });
}

function ingresarDatos(){
    var especialista = $('#especialidad').val();
    var idPersona = $('#doctor').val();
    var data = {"especialidad":especialista,"fechaIngreso":"2015-01-26T19:42:05-05:00","idEmpleado":1,"idPersona":{"idPersona":idPersona},"ocupacion":"doctor"};
    if(especialista!=0 && idPersona>0){
        guardarAsignacion(data);         
    }else{
        alert("Seleccionar los respectivos campos")
    }
}

function comprobarRegistro(cedula, rol, datos){
    var url = server+"persona/exist/"+cedula+"/"+rol;
    
    $.ajax({
        type: 'GET',
        contentType: 'application/json',
        url: url,
        dataType: "text",
        data: {},
        success: function (data, textStatus, jqXHR) {
            if(data>0){
                console.log("Usuario existente");
                alert("El usuario que esta ingresando ya se encuentra registrado...");
            }else{
                console.log("Usuario no existente");
                guardarRegistro(datos);
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log('Ocurrio un error - Reintente Nuevamente ' + jqXHR + textStatus + errorThrown);
        }
    });
}

function guardarRegistro(datos) {
    var url = server+"persona";
    //var data = {"apellidos": apellidoP + " " + apellidoM, "barrio": barrio, "canton": canton, "cedula": cedula, "contrasena": clave, "correo": correo, "fechaNaci": fecha, "idPersona": 3, "nombres": primerNombre + " " + segundoNombre, "parraquia": parroquia, "provincia": provincia, "rol": "paciente", "sexo": sexo, "telefono": telefono, "tipoSangre": sangre, "usuario": usuario};
    var data = datos;
    console.log(data);
    $.ajax({
        type: 'POST',
        contentType: 'application/json',
        url: url,
        dataType: 'json',
        data: JSON.stringify(data),
        success: function (data, textStatus, jqXHR) {
            alert("Datos Guardados correctamente");
            location.href = "javascript:location.reload()";
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert('Ocurrio un error - Reintente Nuevamente ' + jqXHR + textStatus + errorThrown);
        }
    });
}
function guardarAsignacion(datos){
    var url = server+"empleado";
    //var data = {"apellidos": apellidoP + " " + apellidoM, "barrio": barrio, "canton": canton, "cedula": cedula, "contrasena": clave, "correo": correo, "fechaNaci": fecha, "idPersona": 3, "nombres": primerNombre + " " + segundoNombre, "parraquia": parroquia, "provincia": provincia, "rol": "paciente", "sexo": sexo, "telefono": telefono, "tipoSangre": sangre, "usuario": usuario};
    var data = datos;
    console.log(data);
    $.ajax({
        type: 'POST',
        contentType: 'application/json',
        url: url,
        dataType: 'json',
        data: JSON.stringify(data),
        success: function (data, textStatus, jqXHR) {
            alert("Datos Guardados correctamente");
            //location.href = "../../";
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert('Ocurrio un error - Reintente Nuevamente ' + jqXHR + textStatus + errorThrown);
        }
    });
}

function guardarHorarios(datos){
    var url = server+"calendariocitas";
    //var data = {"apellidos": apellidoP + " " + apellidoM, "barrio": barrio, "canton": canton, "cedula": cedula, "contrasena": clave, "correo": correo, "fechaNaci": fecha, "idPersona": 3, "nombres": primerNombre + " " + segundoNombre, "parraquia": parroquia, "provincia": provincia, "rol": "paciente", "sexo": sexo, "telefono": telefono, "tipoSangre": sangre, "usuario": usuario};
    var data = datos;
    console.log(data);
    $.ajax({
        type: 'POST',
        contentType: 'application/json',
        url: url,
        dataType: 'json',
        data: JSON.stringify(data),
        success: function (data, textStatus, jqXHR) {
            alert("Datos Guardados correctamente");
            //location.href = "../../";
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert('Ocurrio un error - Reintente Nuevamente ' + jqXHR + textStatus + errorThrown);
        }
    });
}


function ingresarHorario(){
    
    var doctorN = $('#doctorN').val();
    var idPer = $('#idPer').val();
    var hora_inicio = $('#hora_inicio').val();
    var hora_fin = $('#hora_fin').val();
    var detalle = $('#detalle').val();
    //{"detalle":"Indicaciones","fecha":"2015-01-27T00:00:00-05:00","horaFin":"1970-01-01T03:00-05:00","horaInicio":"1970-01-01T10:10:00-05:00","idCalendario":1,"idEmpleado":{"idEmpleado":1}}
    if(hora_inicio!="" && doctorN>0 && hora_fin!=""){
        hora_inicio = "1970-01-01T"+hora_inicio+":00-05:00";
        hora_fin = "1970-01-01T"+hora_fin+":00-05:00";
        var data = {"detalle":"detalle","fecha":"2015-01-27T00:00:00-05:00","horaFin":"1970-01-01T23:06","horaInicio":"1970-01-01T02:02","idCalendario":1,"idEmpleado":{"idEmpleado":1}};
        var data = {"detalle":detalle,"fecha":"2015-01-27T00:00:00-05:00","horaFin":hora_fin,"horaInicio":hora_inicio,"idCalendario":1,"idEmpleado":{"idEmpleado":doctorN}};
        //var data = {"detalle":detalle,"fecha":"2015-01-27T00:00:00-05:00","horaFin":hora_fin,"horaInicio":hora_inicio,"idCalendario":1,"idEmpleado":{"idEmpleado":doctorN,"idPersona":{"idPersona":2}}};
        //console.log(data);
        guardarHorarios(data);         
    }else{
        alert("Seleccionar los respectivos campos")
    }
}